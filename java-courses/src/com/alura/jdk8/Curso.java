/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alura.jdk8;

/**
 *
 * @author gabrielbmoro
 */
public class Curso {

    String name;
    int alunos;

    public Curso(String name, int alunos) {
        this.name = name;
        this.alunos = alunos;
    }
    
    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }

    public int getAlunos() {
        return alunos;
    }

}
