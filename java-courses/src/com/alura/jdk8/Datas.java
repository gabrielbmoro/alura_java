/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alura.jdk8;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.Period;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author gabrielbmoro
 */
public class Datas {
    
    public static void main(String[] args) {
        
        LocalDate hoje = LocalDate.now();
        
        System.out.println(hoje);
        
        LocalDate proximaOlimpiada = LocalDate.of(2024, Month.JUNE, 5);
        LocalDate proximaDaProximaOlimpiada = proximaOlimpiada.plusYears(4);
        
        int anos = proximaOlimpiada.getYear() - hoje.getYear();
        
        System.out.println(anos);
        
        Period periodo = Period.between(hoje, proximaOlimpiada);
        
        System.out.println(periodo.getDays());
        
        DateTimeFormatter formatador = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        String valorFormatado = proximaDaProximaOlimpiada.format(formatador);
        System.out.println(valorFormatado);

        DateTimeFormatter formatadorComHrs = DateTimeFormatter.ofPattern("dd/MM/yyyy hh:mm");


        LocalDateTime agora = LocalDateTime.now();
        System.err.println(agora.format(formatadorComHrs));
    }
    
}
