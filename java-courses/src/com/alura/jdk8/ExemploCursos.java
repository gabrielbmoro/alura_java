/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alura.jdk8;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 *
 * @author gabrielbmoro
 */
public class ExemploCursos {
    
    public static void main(String[] args) {
        List<Curso> cursos = new ArrayList<Curso>();
        cursos.add(new Curso("Python", 45));
        cursos.add(new Curso("JavaScript", 150));
        cursos.add(new Curso("Java 8", 113));
        cursos.add(new Curso("C", 55));
        
        cursos.sort(Comparator.comparing(Curso::getAlunos));
        
        //cursos.forEach(System.out::println);
        
        /*cursos.stream()
                .filter(c -> c.getAlunos() >= 100)
                .forEach(c -> System.out.println(c.getName()));*/
        
       /* cursos.stream()
                .filter(c -> c.getAlunos() >= 100)
                .mapToInt(Curso::getAlunos)
                .forEach(System.out::println);*/
        
       int sum = cursos.stream()
                .filter(c -> c.getAlunos() >= 100)
                .mapToInt(Curso::getAlunos)
                .sum();
        
        System.err.println(sum);
        
        Optional<Curso> optionalCurso = cursos.stream()
                .filter(c -> c.getAlunos() >= 100)
                .findAny();
        
        Curso cursoTmp = optionalCurso.orElse(null);
        System.out.println(cursoTmp.getName()); //ifpresent dá pra usar tbm
        
        
        /*cursos = cursos.stream()
                .filter(c -> c.getAlunos() >= 100)
                .collect(Collectors.toList());*/
        
        Map<String, Integer> map = cursos.parallelStream()
                .filter(c -> c.getAlunos() >= 100)
                .collect(Collectors.toMap(
                        c -> c.getName(),
                        c -> c.getAlunos()
                ));
                
        map.forEach((nome, alunos) -> System.out.println(nome + " tem " + alunos + " alunos"));
        
        
    }

}
