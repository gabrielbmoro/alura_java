/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.alura.jdk8;

import java.util.List;
import java.util.ArrayList;
import java.util.Comparator;

public class OrdenaStrings {

    public static void main(String[] args) {

        List<String> palavras = new ArrayList<String>();
        palavras.add("alura online");
        palavras.add("editora casa do código");
        palavras.add("caelum");

        /*Default methods, interfaces podem possuir methodos implementados*/
        palavras.sort((o1, o2) -> Integer.compare(o1.length(), o2.length()));

        System.out.println(palavras);
        
        /*Pra usar lâmida apenas usa-se com interfaces que possuam apenas um método 
        abstrato, os outros podem até existir, mas devem ser default(funcionais)*/
        
        palavras.forEach(s -> System.out.println(s));
        
        
        
        palavras.sort(Comparator.comparing(s -> s.length()));
         palavras.sort(Comparator.comparing(String::length)); //method reference é uma forma mais elegante para escrever labmdas

    }

}
