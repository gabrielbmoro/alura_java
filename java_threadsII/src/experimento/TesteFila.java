/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package experimento;

import java.util.LinkedList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 *
 * @author gabrielbmoro
 */
public class TesteFila {
    
    public static void main(String[] args) throws InterruptedException {
        
        BlockingQueue<String> fila = new ArrayBlockingQueue<>(3);
        
        fila.put("c1");
        fila.put("c2");
        fila.put("c3");
        fila.put("c4"); //já trava aqui com put diferente de utilizar offer
        
        System.out.println("Size: " + fila.size());
        
        System.out.println(fila.take());
        System.out.println(fila.take());
        System.out.println(fila.take());
        System.out.println(fila.take()); //bloqueou até conseguir um novo cara
        
        System.out.println("Size: " + fila.size());
        
    }
    
}
