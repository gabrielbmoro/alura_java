/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clientserver.server;

import java.io.PrintStream;
import java.util.Random;
import java.util.concurrent.Callable;

/**
 *
 * @author gabrielbmoro
 */
public class ComandoAcessaBanco implements Callable<String> {

    private PrintStream saida;
    
    public ComandoAcessaBanco(PrintStream saida) {
        this.saida = saida;
    }
    
    @Override
    public String call() throws Exception {
        System.out.println("Servidor: Executando acesso ao banco de dados ");
        
        saida.println("Servidor: Acesso ao banco de dados executado com sucesso!");
        
        Thread.sleep(20000);
        
        int numero = new Random().nextInt(100) + 1;
        
        System.out.println("Servidor: finalizou o acesso ao banco de dados ");
        
        return Integer.toString(numero);
        
    }
    
}
