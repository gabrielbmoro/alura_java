/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clientserver.server;

import java.io.PrintStream;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 *
 * @author gabrielbmoro
 */
public class JuntaResultadosWSEBanco implements Callable<Void>{

    private Future<String> futureWS;
    private Future<String> futureBanco;
    private PrintStream saidaCliente;
    
    public JuntaResultadosWSEBanco(Future<String> futureWS, Future<String> futureBanco, 
            PrintStream saidaCliente) {
        this.futureWS = futureWS;
        this.futureBanco = futureBanco;
        this.saidaCliente = saidaCliente;
    }
    
    @Override
    public Void call() throws Exception {
        
        try {
            
            System.out.println("Aguardando resultados do future WS e banco");

            String numeroMagico = this.futureWS.get(40, TimeUnit.SECONDS);
            String numeroMagico2 = this.futureBanco.get(48, TimeUnit.SECONDS);
            
            this.saidaCliente.println("Resutlado comando other... numeros mágicos: " + numeroMagico + ", " + numeroMagico2);
            
        } catch(InterruptedException | ExecutionException | TimeoutException e) {
            this.saidaCliente.println("Execução do comando other timeout");
            this.futureWS.cancel(true);
            this.futureBanco.cancel(true);
        }
          
        
        return null;
    }
    
}
