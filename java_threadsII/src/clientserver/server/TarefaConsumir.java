/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clientserver.server;

import java.util.concurrent.BlockingQueue;

/**
 *
 * @author gabrielbmoro
 */
public class TarefaConsumir implements Runnable {

    private BlockingQueue<String> filaComandos;

    public TarefaConsumir(BlockingQueue<String> filaComandos) {
        this.filaComandos = filaComandos;
    }

    @Override
    public void run() {
        try {
            String comando = null;

            while ((comando = filaComandos.take()) != null) {
                System.out.println("Servidor: consumindo comando c3 " + Thread.currentThread().getName());

                Thread.sleep(2000);
            }

        } catch (InterruptedException erro) {
            throw new RuntimeException(erro);
        }
    }

}
