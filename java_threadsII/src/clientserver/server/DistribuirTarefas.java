/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clientserver.server;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author gabrielbmoro
 */
public class DistribuirTarefas implements Runnable {

    private Socket socket;
    private ServidorTarefas servidor;
    private ExecutorService threadPool;
    private BlockingQueue<String> filaComandos;

    public DistribuirTarefas(ExecutorService threadPool, BlockingQueue<String> filaComandos , Socket socket, ServidorTarefas servidor) {
        this.socket = socket;
        this.servidor = servidor;
        this.threadPool = threadPool;
        this.filaComandos = filaComandos;
    }

    @Override
    public void run() {

        System.err.println("Distribuindo tarefas para " + socket.hashCode());

        try {
            Scanner entradaDeClient = new Scanner(socket.getInputStream());

            PrintStream saidaCliente = new PrintStream(socket.getOutputStream());

            while (entradaDeClient.hasNextLine()) {
                String comando = entradaDeClient.nextLine();
                System.err.println("Servidor: Comando recebido -> " + comando + " (id: " + socket.hashCode() + ")");

                switch (comando) {
                    case "c1": {
                        saidaCliente.println("Servidor: Confirmação do comando c1" + " (id: " + socket.hashCode() + ")");
                        threadPool.execute(new Comando(saidaCliente, "c1"));
                        break;
                    }
                    case "c2": {
                        saidaCliente.println("Servidor: Confirmação do comando c2" + " (id: " + socket.hashCode() + ")");
                        threadPool.execute(new Comando(saidaCliente, "c2"));
                        break;
                    }
                    case "c3": {
                        this.filaComandos.put(comando);
                        saidaCliente.println("Servidor: Comando c3 adicionado na fila ");
                    }
                    case "fim": {
                        saidaCliente.println("Servidor: Confirmação do comando fim" + " (id: " + socket.hashCode() + ")");
                        threadPool.execute(new Comando(saidaCliente, "fim"));
                        servidor.parar();
                        break;
                    }
                    default: {
                        saidaCliente.println("Servidor: Comando não encontrado" + " (id: " + socket.hashCode() + ")");
                        Future<String> futureWS = threadPool.submit(new ComandoChamaWS(saidaCliente));
                        Future<String> futureBanco = threadPool.submit(new ComandoAcessaBanco(saidaCliente));
                        
                        this.threadPool.submit(new JuntaResultadosWSEBanco(futureWS, futureBanco, saidaCliente));
                    }
                }
            }

        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (InterruptedException ex) {
            Logger.getLogger(DistribuirTarefas.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
