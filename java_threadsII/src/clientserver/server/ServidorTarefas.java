package clientserver.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author gabrielbmoro
 */
public class ServidorTarefas {

    private ServerSocket servidor; 
    private ExecutorService threadPool;
    private AtomicBoolean estaRodando;
    private BlockingQueue<String> filaComandos;
    
    public ServidorTarefas() throws IOException{
        System.out.println("Servidor: Iniciando o serviço...");
        this.servidor = new ServerSocket(12345);
        this.threadPool = Executors.newCachedThreadPool(new FabricaDeThreads());
        //this.threadPool = Executors.newFixedThreadPool(4, new FabricaDeThreads());
        this.estaRodando = new AtomicBoolean(true);
        this.filaComandos = new ArrayBlockingQueue<>(2);
        iniciarConsumidores();
    }
    
    public static void main(String[] args) throws IOException, InterruptedException {

        ServidorTarefas servidor = new ServidorTarefas();
        servidor.rodar();
        servidor.parar();
        
        //ExecutorService threadPool = Executors.newFixedThreadPool(2);
        
        
       
    }
    
    public void rodar() {
        while (this.estaRodando.get()) {
            
        try{
                Socket socket = servidor.accept();

                System.err.println("Servidor: Cliente aceito na porta "+socket.getPort());

                DistribuirTarefas distribuirTarefas = new DistribuirTarefas(threadPool, filaComandos, socket, this);

                Thread threadClient = new Thread(distribuirTarefas);
                threadPool.execute(threadClient);

            
        }catch(SocketException e) {
            System.out.println("SocketExeption, Está rodando?" +  this.estaRodando);
        } catch(IOException erro2) {
            erro2.printStackTrace();
        }
        }
    }
    
    public void parar() throws IOException {
        this.estaRodando.set(false);
        servidor.close();
        threadPool.shutdown();
    }

    private void iniciarConsumidores() {
        
        int quantidadeDeConsumidores = 2;
                
        for(int i = 0; i < quantidadeDeConsumidores; i++) {
            TarefaConsumir tarefa = new TarefaConsumir(filaComandos);
            this.threadPool.execute(tarefa);
        }
    }
    
}
