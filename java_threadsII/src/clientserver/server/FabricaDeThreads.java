/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clientserver.server;

import java.util.concurrent.ThreadFactory;

/**
 *
 * @author gabrielbmoro
 */
public class FabricaDeThreads implements ThreadFactory {

    static int threadId = 0;
    
    @Override
    public Thread newThread(Runnable r) {
        
        threadId++;
        
        Thread thread = new Thread(r, "Thread Servidor de Tarefas " + threadId);
        
        thread.setUncaughtExceptionHandler(new TratadorDeExecao());
        
        return thread;
    }
   
    
}
