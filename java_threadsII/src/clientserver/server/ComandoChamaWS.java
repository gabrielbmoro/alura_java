/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clientserver.server;

import java.io.PrintStream;
import java.util.Random;
import java.util.concurrent.Callable;

/**
 *
 * @author gabrielbmoro
 */
public class ComandoChamaWS implements Callable<String> {

    private PrintStream saida;
    
    public ComandoChamaWS(PrintStream saida) {
        this.saida = saida;
    }
    
    @Override
    public String call() throws Exception {
        System.out.println("Servidor: Executando chamada a WS");
        
        saida.println("Servidor: Chamada a WS executada com sucesso!");
        
        Thread.sleep(20000);
        
        int numero = new Random().nextInt(100) + 1;
        
        System.out.println("Servidor: finalizou chamada a WS ");
        
        return Integer.toString(numero);
        
    }
    
}
