/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clientserver.server;

import java.io.PrintStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author gabrielbmoro
 */
public class Comando implements Runnable {

    private PrintStream saida;
    private String command;
    
    public Comando(PrintStream saida, String command) {
        this.saida = saida;
        this.command = command;
    }
    
    @Override
    public void run() {
        System.out.println("Servidor: Executando comando " + command);
        
        try {
            Thread.sleep(20000);
        } catch (InterruptedException ex) {
            throw new RuntimeException(ex);
        }
        
        if(command.equalsIgnoreCase("c2")) {
            throw new RuntimeException("pauleou");
        }
        
        saida.println("Servidor: Comando " + command + " executado com sucesso!");
    }
    
}
