package clientserver.client;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author gabrielbmoro
 */
public class ClienteTarefas {

    /**
     *
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {

        Socket socket = new Socket("localhost", 12345);

        System.out.println("Cliente: Conexão estabelecida");

        Thread enviaComando = new Thread(new Runnable() {
            @Override
            public void run() {
                PrintStream saida = null;
                try {
                    saida = new PrintStream(socket.getOutputStream());
                    Scanner teclado = new Scanner(System.in);
                    System.out.println("Cliente: Pode enviar ao servidor");

                    
                    while (teclado.hasNextLine()) {
                        String linha = teclado.nextLine();

                        if (linha.trim().equals("")) {
                            break;
                        }

                        saida.println(linha);

                    }
                    saida.close();
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
            }
        });

        Thread recebeResposta = new Thread(new Runnable() {
            @Override
            public void run() {

                PrintStream saida = null;

                try {
                    saida = new PrintStream(socket.getOutputStream());
                    Scanner teclado = new Scanner(System.in);

                    System.out.println("Cliente: Recebendo resposta do servidor");
                    Scanner respostaDoServidor = new Scanner(socket.getInputStream());

                    while (respostaDoServidor.hasNextLine()) {
                        String linha = respostaDoServidor.nextLine();
                        System.out.println(linha);
                    }
                    saida.close();
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
            }
        });

        recebeResposta.start();
        enviaComando.start();
      
        enviaComando.join();
        
        System.out.println("Cliente: Fechando socket do cliente");
        socket.close();
    }

}
