/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.alura.banheiro;

/**
 *
 * @author gabrielbmoro
 */
public class TarefaLimpeza implements Runnable {

    private Banheiro banheiro;

    public TarefaLimpeza(Banheiro banheiro) {
        this.banheiro = banheiro;

    }

    @Override
    public void run() {
        while (true) {
            banheiro.limpa();
            banheiro.dormeUmPouco(20000);
        }
    }
}
