/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.alura.lista;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author gabrielbmoro
 */
public class TarefaAdicionarElemento implements Runnable{

    private final List<String> lista;
    private final int numeroDaThread;

    
    public TarefaAdicionarElemento(List<String> lista, int numeroDaThread){
        this.lista = lista;
        this.numeroDaThread = numeroDaThread;
    }
    
    @Override
    public void run() {
        for(int i = 0; i < 10; i++) {
            lista.add("Thread "+ numeroDaThread + ": " + i);
        }
    }
    
}
