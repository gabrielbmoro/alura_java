/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.alura.lista;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

/**
 *
 * @author gabrielbmoro
 */
public class Main {
    
    public static void main(String[] args) {
        
        //List<String> lista = Collections.synchronizedList(new ArrayList<String>());
        List<String> lista = new Vector<String>();
        
        for(int i = 0; i < 10; i++) {
            Thread thread = new Thread(new TarefaAdicionarElemento(lista, i));
            thread.start();
        }
        
        try {
        Thread.sleep(5000);
        }catch(InterruptedException e){};
        
        for(int x = 0; x < lista.size(); x++) {
            System.out.println(lista.get(x));
        }
    }
    
}
