/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.alura.threads.lista;

/**
 *
 * @author gabriel-moro
 */
public class TarefaImprimir implements Runnable {

    private Lista lista;
    
    public TarefaImprimir(Lista lista) {
        this.lista = lista;
    }
    
    @Override
    public void run() {
        
        synchronized(lista) {
            try{
                System.err.println("Tarefa Imprimir - Indo dormir");
                lista.wait();
            }catch(InterruptedException e) {
                e.printStackTrace();
            }
        }
        
        for(int i = 0; i < lista.tamanho(); i++) {
            System.out.println(i + " - " + lista.pegaElemento(i));
        }
        
    }
    
}
