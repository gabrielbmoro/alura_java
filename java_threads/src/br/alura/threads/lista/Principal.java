/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.alura.threads.lista;


/**
 *
 * @author gabriel-moro
 */
public class Principal {

    public static void main(String[] args) throws InterruptedException{

        Lista lista = new Lista();

        for (int i = 0; i < 10; i++) {
            new Thread(new TarefaAdicionarElemento(lista, i)).start();
        }
        
        Thread.sleep(2000);
        
        new Thread(new TarefaImprimir(lista)).start();
    
    }
    

}
