/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.alura.threads.lista;

/**
 *
 * @author gabriel-moro
 */
public class Lista {

    private String[] elementos = new String[100];
    private int indice = 0;

    public synchronized void adicionaElementos(String elemento) {
        this.elementos[indice] = elemento;
        indice++;

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (this.indice == this.elementos.length) {
            System.err.println("lista ta cheia - notificando a galera...");
            this.notify();
        }

    }

    public int tamanho() {
        return this.elementos.length;
    }

    public String pegaElemento(int position) {
        return this.elementos[position];
    }

}
