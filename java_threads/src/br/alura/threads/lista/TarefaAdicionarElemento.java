/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.alura.threads.lista;

/**
 *
 * @author gabriel-moro
 */
public class TarefaAdicionarElemento implements Runnable {

    private Lista lista;
    private int numeroDaThread;

    public TarefaAdicionarElemento(Lista lista, int numeroDaThread) {
        this.lista = lista;
        this.numeroDaThread = numeroDaThread;
    }

    @Override
    public void run() {
        for (int i = numeroDaThread; i < (numeroDaThread + 10); i++) {
            System.out.println("Adicionando elemento - " + i);
            lista.adicionaElementos("Thread " + numeroDaThread + " - " + i);
        }
    }

}
