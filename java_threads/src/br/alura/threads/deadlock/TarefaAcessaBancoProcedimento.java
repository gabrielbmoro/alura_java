/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.alura.threads.deadlock;

/**
 *
 * @author gabriel-moro
 */
public class TarefaAcessaBancoProcedimento implements Runnable {

    private PoolDeConexao pool;
    private GerenciadorDeTransacao tx;

    public TarefaAcessaBancoProcedimento(PoolDeConexao pool,
            GerenciadorDeTransacao tx) {
        this.pool = pool;
        this.tx = tx;
    }

    @Override
    public void run() {
// dentro do método run da classe TarefaAcessaBancoProcedimento
        synchronized (pool) {

            System.out.println("Peguei a chave da pool");
            pool.getConnection();

            synchronized (tx) {

                System.out.println("Peguei a chave do tx");
                tx.begin();
            }
        }
    }
}
