/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.alura.threads.banheiro;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author gabriel-moro
 */
public class Banheiro {

    private boolean ehSujo = true;

    public void fazNumero1() {
        String nome = Thread.currentThread().getName();
        System.out.println(nome + ": batendo na porta");
        synchronized (this) {

            System.out.println(nome + ": entrando no banheiro");

            while (ehSujo) {
                esperaLaFora(nome);
            }

            System.out.println(nome + ": fazendo coisa rápida");

            dormeUmPouco(10000);

            this.ehSujo = true;

            System.out.println(nome + ": dando descarga");
            System.out.println(nome + ": lavando sommm");
            System.out.println(nome + ": saindo do banheiro");

        }
    }

    public void fazNumero2() {
        String nome = Thread.currentThread().getName();
        System.out.println(nome + ": batendo na porta");

        synchronized (this) {

            System.out.println(nome + ": entrando no banheiro");

            while (ehSujo) {
                esperaLaFora(nome);
            }

            System.out.println(nome + ": fazendo coisa demorada");

            dormeUmPouco(10000);

            this.ehSujo = true;

            System.out.println(nome + ": dando descarga");
            System.out.println(nome + ": lavando sommm");
            System.out.println(nome + ": saindo do banheiro");
        }
    }

    public synchronized void limpa() {
        String nome = Thread.currentThread().getName();
        System.out.println(nome + ": batendo na porta");

        synchronized (this) {

            System.out.println(nome + ": entrando no banheiro");

            if (!ehSujo) {
                System.out.println(nome + ": não está sujo, vou sair!");
                return;
            }

            System.out.println(nome + ": limpando banheiro");
            this.ehSujo = false;

            dormeUmPouco(10000);

            this.notifyAll();
        }
    }

    private void esperaLaFora(String nome) {
        System.out.println(nome + ", eca, banheiro tá sujo!");
        try {
            this.wait();
        } catch (InterruptedException ex) {
            Logger.getLogger(Banheiro.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void dormeUmPouco(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
