/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.alura.threads.banheiro;

/**
 *
 * @author gabriel-moro
 */
public class TarefaNumero2 implements Runnable{

    private Banheiro banheiro;

    public TarefaNumero2(Banheiro banheiro){
        this.banheiro = banheiro;
        
    }
    
    @Override
    public void run() {
        banheiro.fazNumero2();
    }
    
}
