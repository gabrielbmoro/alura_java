/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.alura.threads.banheiro;

/**
 *
 * @author gabriel-moro
 */
public class Principal {
    
    public static void main(String[] args) {

        
        Banheiro banheiro = new Banheiro();
        
        Thread thread1 = new Thread(new TarefaNumero1(banheiro), "João");
        Thread thread2 = new Thread(new TarefaNumero2(banheiro), "Pedro");
        Thread thread3 = new Thread(new TarefaNumero2(banheiro), "Mariana");
        Thread thread4 = new Thread(new TarefaNumero2(banheiro), "Joana");
        Thread thread5 = new Thread(new TarefaLimpeza(banheiro), "Limpeza");
        thread5.setDaemon(true); //vive apenas quando estiver threads
        
        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();
        thread5.start();
        
    }
    
}
