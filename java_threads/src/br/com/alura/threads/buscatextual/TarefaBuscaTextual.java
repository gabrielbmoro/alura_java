/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.alura.threads.buscatextual;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author gabriel-moro
 */
public class TarefaBuscaTextual implements Runnable{

    private final String nomeDoArquivo;
    private final String nome;

    public TarefaBuscaTextual(String nomeDoArquivo, String nome) {
        this.nomeDoArquivo = nomeDoArquivo;
        this.nome = nome;
        
    }
    
    @Override
    public void run() {
        try {
            Scanner scanner = new Scanner(new File(nomeDoArquivo));
            
            int numberLine=1;
            
            while(scanner.hasNextLine()) {
                String line = scanner.nextLine();
                
                if(line.toLowerCase().contains(nome.toLowerCase())) {
                    System.out.println(nomeDoArquivo + "-" + numberLine + ": "+ line);
                }
                numberLine++;
            }
            
        } catch (FileNotFoundException ex) {
            throw new RuntimeException(ex.getMessage());
        }
    }
    
}
