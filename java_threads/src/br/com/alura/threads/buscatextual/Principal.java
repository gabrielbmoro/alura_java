/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.alura.threads.buscatextual;

import java.net.URL;

/**
 *
 * @author gabriel-moro
 */
public class Principal {
    
    public static void main(String[] args) {
                
        String nome = "ja";
        
        Thread threadAssinatura1 = new Thread(new TarefaBuscaTextual("/home/gabriel-moro/Downloads/nomes/assinaturas1.txt",nome));
        Thread threadAssinatura2 = new Thread(new TarefaBuscaTextual("/home/gabriel-moro/Downloads/nomes/assinaturas2.txt",nome));
        Thread threadAutores = new Thread(new TarefaBuscaTextual("/home/gabriel-moro/Downloads/nomes/autores.txt",nome));
    
        threadAssinatura1.start();
        threadAssinatura2.start();
        threadAutores.start();
        
    }
}
